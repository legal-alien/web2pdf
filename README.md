# Web2Pdf

**URL => NodeJS, Express, Puppeteer => PDF**

Convert the Web page to PDF with the URL as an argument.

## Getting Started

Clone this repository

```bash
$ git clone https://gitlab.com/leagal-alien/web2pdf.git

 :
$ cd web2pdf/src
```

npm install

```bash
$ npm install
```

npm start

```bash
$ npm start

```

Open `localhost: 3000` in the browser and give the URL of the website you want to be pdf in the parameter.

```
http://localhost:3000/?url=<The URL of the website you want to be pdf.>
```

It's better to `urlencode` the URL string.

### Prerequisites

**[puppeteer](https://github.com/GoogleChrome/puppeteer)**

I am using puppeteer. We need an environment in which puppeteer works.

I used Ubuntu 18.04, but I got an error unless I installed something that seems to be related to gtk + 3.0.

```bash
$ sudo apt-get install libx11-xcb1 libxcomposite-dev libxcursor-dev libxdamage-dev libxi-dev libxtst-dev libcups2-dev libxss-dev libxrandr-dev libasound2 libpangocairo-1.0-0 libatk1.0-dev libatk-bridge2.0-dev libgtk-3-0
```

I think that the above will make a difference depending on the environment.

I wanted to convert Japanese web site, so I also included the following.

```bash
$ sudo apt-get install -y language-pack-ja-base language-pack-ja
$ sudo apt-get install -y 'fonts-takao-*'
```

**The version of NodeJS  npm Express when I made it was below.**

```bash
$ node --version
v11.2.0
```

```bash
$ npm --version
6.4.1
```

```bash
$ express --version
4.16.0
```

I used express-generator.


### Production process

**express**

```bash
$ express --git --ejs web2pdf
```

The directory called `web2pdf` was later changed to the name `src`.

**Installation of puppeteer**

```bash
$ npm i -S puppeteer
```

**`routes/index.js` remodeled**

Please look at `routes/index.js` in this repository. It is in the `src` directory.

Unnecessary view and routing may be deleted. For example, `route/users.js`.

## License

This is a sample, so please feel free.

