var express = require('express');
var router = express.Router();
const puppeteer = require('puppeteer');

/* GET home page. */
router.get('/', function(req, res, next) {
  //res.render('index', { title: 'Express' });
  // console.log(req.query);
  if (req.query.url) {
    try {
		  (async () => {
			  const browser = await puppeteer.launch({ 
          headless: true,
          args: ['--no-sandbox', '--disable-setuid-sandbox']
        });
			  const page = await browser.newPage();
			  await page.goto(req.query.url, {waitUntil: 'networkidle2'});
			  //const ret_pdf = await page.pdf({path: 'hn.pdf', format: 'A4'});
			  const ret_pdf = await page.pdf({format: 'A4'});
			  await browser.close();

        res.setHeader('Content-disposition', 'attachment; filename="download.pdf"');
        res.setHeader('Content-type', 'application/pdf');
			  res.send(ret_pdf);
		  })();
    } catch (err) {
      var param = {"messgae":"puppeteer err","値":err};
      res.header('Content-Type', 'application/json; charset=utf-8')
      res.send(param);
    }
  }else{
    var param = {"messgae":"パラメータ不正","値":req.query};
    res.header('Content-Type', 'application/json; charset=utf-8')
    res.send(param);
  }
});

module.exports = router;

